package com.example.beatbox;

public class Sound {
    private  String mAssetPath;
    private  String mName;

    public Sound(String mAssetPath) {
        this.mAssetPath = mAssetPath;
        String[] components=mAssetPath.split("/");
        String filename=components[components.length-1];
        mName=filename.replace(".wav","");
    }

    public String getmAssetPath() {
        return mAssetPath;
    }

    public String getmName() {
        return mName;
    }
}
