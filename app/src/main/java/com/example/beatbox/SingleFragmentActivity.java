package com.example.beatbox;

import android.os.Bundle;

import androidx.annotation.LayoutRes;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

public abstract class SingleFragmentActivity extends AppCompatActivity {
    protected  abstract Fragment createFragment();
    /**
     * SingleFragmentActivity现在只能实例化activity_frament.xml布局
     * 为了使SingleFramentActivity更加好用，我们让子类自己提供资源ID
     */
    @LayoutRes
    protected  int getLaoutResId(){
        return R.layout.activity_fragment;
    }
    protected  void  onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
       // setContentView(R.layout.activity_fragment);
        setContentView(getLaoutResId());
        FragmentManager fm=getSupportFragmentManager();
        Fragment fragment=fm.findFragmentById(R.id.fragment_container);
        if(fragment==null){
            fragment=createFragment();
            fm.beginTransaction()
                    .add(R.id.fragment_container,fragment)
                    .commit();
        }
    }
}
