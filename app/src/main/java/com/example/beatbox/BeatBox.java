package com.example.beatbox;

import android.content.Context;
import android.content.res.AssetManager;
import android.util.Log;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

//处理assets资源
public class BeatBox {
    private  static  final String TAG="BeatBox";
    private  static  final String SOUNDS_FOLDER="sample_sounds";
    private List<Sound> mSound=new ArrayList<>();

    private AssetManager mAssets;
    public  BeatBox(Context context){
        mAssets=context.getAssets();
        loadSounds();
    }
    private  void loadSounds(){
        String[] soundNames;
        try{
            soundNames =mAssets.list(SOUNDS_FOLDER);
            Log.d(TAG, "loadSounds: Found"+soundNames.length+"sounds");

        }catch (IOException ioe){
            Log.d(TAG, "loadSounds: cound not list assets",ioe);
            return;
        }

        for (String filename:soundNames){
            String  assetPath=SOUNDS_FOLDER+"/"+filename;
            Sound sound=new Sound(assetPath);
            mSound.add(sound);
        }
    }

    public List<Sound> getmSound() {
        return mSound;
    }
}
